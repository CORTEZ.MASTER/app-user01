package com.example.cortez.myapplicarion;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView img;
    ImageView img2;
    ImageView img3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Deixar celular somente na vertical
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Instanciando a imagem

        //abrir tela de usuario ja cadastrado
        img = (ImageView) findViewById(R. id. imageView2);

        img.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                Intent i = new Intent( MainActivity.this, Main2Activity.class);
                startActivity(i);
            }
        });

        //abrir tela de adicionar novo usuario
        img2 = (ImageView) findViewById(R.id.imageView3);

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent( MainActivity.this, Main3Activity.class);
                startActivity(i);
            }
        });

        //abrir tela de localização
        img3 = (ImageView) findViewById(R.id.imageView4);

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent( MainActivity.this, Main4Activity.class);
                startActivity(i);
            }
        });
    }
}
